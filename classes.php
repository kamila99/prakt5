<?php
class emitent {
public $name;
public $cost; //текущая цена
public $trand; //направление тренда
public $closeCost; //цена закрытия
public $average50; //значение скользящей средней 50 дней
public $average15; //значение скользящей средней 15 дней
                        
public function getPrice($emitent,$format)
{
    $uri = 'https://iss.moex.com/iss/engines/stock/markets/shares/securities/'.$emitent.$format;
    $result = file_get_contents($uri);
    $resultObject = json_decode($result,true);
    /* @var $resultObject type */
    $this->cost = $resultObject['marketdata']['data'][2][12];
    return $this->cost;
}

public function getAverage50($emitent,$format)
{
    $historyPrice = [];
    $historydata = [];
    $date1 = date("y-m-d",strtotime("-1 days"));
    $date2 = date("y-m-d",strtotime("-100 days"));
    $uri = 'https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/TQBR/securities/'.$emitent.$format.'?from='.$date2.'&till='.$date1;
    $result = file_get_contents($uri);
    $resultObject = json_decode($result,true);
    $historydata = $resultObject['history']['data'];
    $historydata_reverse = array_reverse($historydata);
    for ($i=0;$i<50;$i++)
     {
      array_push($historyPrice,$historydata_reverse[$i][11]);
     }
    $average50 = array_sum($historyPrice)/count($historyPrice);
    return $average50;
}

public function getAverage15($emitent,$format)
{
 $historyPrice = [];
 $historydata = [];
 $date1 = date("y-m-d",strtotime("-1 days"));
 $date2 = date("y-m-d",strtotime("-50 days"));
 $uri = 'https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/TQBR/securities/'.$emitent.$format.'?from='.$date2.'&till='.$date1;
 $result = file_get_contents($uri);
 $resultObject = json_decode($result,true);
 $historydata = $resultObject['history']['data'];
 $historydata_reverse = array_reverse($historydata);
 for ($i=0;$i<15;$i++)
  {
   array_push($historyPrice,$historydata_reverse[$i][11]);
  }
 $average15 = array_sum($historyPrice)/count($historyPrice);
 return $average15;  
}

}